const express = require('express');
const { findIndex } = require ('lodash');

const app = express();
const port = 4000;

const todos = [{
	id: "1",
	title: 'grocery shopping',
	completed: false
}, {
	id: "2",
	title: 'get window chalk from homedepot',
	completed: true
}];

app.use(express.json());

app.get('/', (request, response) => {
	response.send('smoke test');
})

app.get('/todos', (request, response) => {
	response.send({ todos });
})

app.put('/todo/:id', (request, response) => {
	const id = request.params.id;
	const todo = {
		...request.body.todo,
		id
	};
	
	const index = findIndex(todos, { id });
	todos.splice(index, 1, todo);
	
	response.send({ todo });
})

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
})
